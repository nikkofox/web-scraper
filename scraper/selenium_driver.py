import os
from abc import abstractmethod

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from output import Output
from scraper.basic import BasicScraper, SupportedEngine


class Selenium(BasicScraper):
    def __init__(self, query: str, quantity: int, recursive: bool = False, output: Output = None):
        super().__init__(query, quantity, recursive, output)
        self.driver = webdriver.Firefox(service_log_path=os.path.devnull)
        self.driver.implicitly_wait(20)

    @abstractmethod
    def find_links(self):
        pass

    @abstractmethod
    def next_page(self):
        pass

    def find_links_from_url(self, url: str):
        self.driver.get(url)
        for a in self.driver.find_elements_by_tag_name('a'):
            if len(self.output) == self.quantity:
                break

            current_link = a.get_attribute('href') or ''
            if self._is_link_correct(current_link) and a.text:
                self.output.add(a.text, current_link)
                self.unique_links.add(current_link)
                self.find_links_from_url(current_link)

    def __del__(self):
        self.driver.quit()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.driver.quit()


class GoogleSelenium(Selenium):
    def next_page(self):
        self.driver.find_element_by_xpath("//a[@id='pnnext']").click()

    def search(self):
        self.driver.get(SupportedEngine.google.value)
        self.driver.find_element_by_name("q").send_keys(self.query)
        self.driver.find_element_by_name("q").send_keys(Keys.ENTER)
        super().search()

    def find_links(self):
        for g_class in self.driver.find_elements_by_class_name('g'):
            if self.quantity == len(self.output):
                break

            a = g_class.find_element_by_tag_name('a')
            h3 = a.find_elements_by_tag_name("h3")
            if a.get_attribute('href') and h3:
                text = h3[0].text
                self.output.add(text, a.get_attribute('href'))
                if self.recursive:
                    self.find_links_from_url(a.get_attribute('href'))


class DuckDuckGoSelenium(Selenium):
    def next_page(self):
        self.driver.find_element_by_xpath("//input[@value='Next']").click()

    def search(self):
        self.driver.get(SupportedEngine.duckduckgo.value)
        self.driver.find_element_by_id("search_form_input_homepage").send_keys(self.query)
        self.driver.find_element_by_id("search_form_input_homepage").send_keys(Keys.ENTER)
        super().search()

    def find_links(self):
        for a in self.driver.find_elements_by_xpath("//h2/a[contains(@class, 'result__a')]"):
            if self.quantity == len(self.output):
                break

            if a.get_attribute('href'):
                self.output.add(a.text, a.get_attribute('href'))
                if self.recursive:
                    self.find_links_from_url(a.get_attribute('href'))
