import argparse

from output import Console, CSV, JSON, SQLite, Output
from scraper import GoogleRequests, GoogleSelenium, DuckDuckGoRequests, DuckDuckGoSelenium
from scraper.basic import BasicScraper

SUPPORTED_ENGINE = {'selenium': {'google': GoogleSelenium, 'duckduckgo': DuckDuckGoSelenium},
                    'requests': {'duckduckgo': DuckDuckGoRequests, 'google': GoogleRequests}}


def get_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Collecting links for a search query "
                                                 "and saving the result in various formats")
    parser.add_argument('query',
                        nargs='+',
                        help="search query",
                        type=str)
    parser.add_argument('-d', '--driver',
                        choices=SUPPORTED_ENGINE.keys(),
                        help="web scraper driver",
                        default='requests')
    parser.add_argument('-e', '--engine',
                        choices=['duckduckgo', 'google'],
                        help="search engine",
                        default='duckduckgo')
    parser.add_argument('-q', '--quantity',
                        type=int,
                        help="number of links",
                        default=20)
    parser.add_argument('-r', '--recursive',
                        action='store_true',
                        help="search links recursively")

    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-c', '--csv',
                       action='store_true',
                       help="output of the result in the .csv file")
    group.add_argument('-j', '--json',
                       action='store_true',
                       help="output of the result in the .json file")
    group.add_argument('-s', '--sqlite',
                       action='store_true',
                       help="output of the result in the sqlite database")

    return parser.parse_args()


def get_output_format(csv: bool, json: bool, sqlite: bool, filename: str) -> Output:
    output = Console(continuously_write=True)
    if csv:
        output = CSV(filename)
    elif json:
        output = JSON(filename)
    elif sqlite:
        output = SQLite(filename)
    return output


def init_scraper(args) -> BasicScraper:
    output = get_output_format(args.csv, args.json, args.sqlite, '_'.join(args.query))
    scraper = SUPPORTED_ENGINE[args.driver][args.engine](' '.join(args.query), args.quantity, args.recursive, output)
    return scraper


def main():
    try:
        scraper = init_scraper(get_args())
        scraper.search()
        scraper.output.write()
    except KeyboardInterrupt:
        print('Exit with KeyboardInterrupt')


if __name__ == '__main__':
    main()
